import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JFamilyAppPersonModule } from './person/person.module';
import { JFamilyAppRelativeModule } from './relative/relative.module';
import { JFamilyAppFamilyModule } from './family/family.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        JFamilyAppPersonModule,
        JFamilyAppRelativeModule,
        JFamilyAppFamilyModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JFamilyAppEntityModule {}
