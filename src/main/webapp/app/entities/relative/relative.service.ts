import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRelative } from 'app/shared/model/relative.model';

type EntityResponseType = HttpResponse<IRelative>;
type EntityArrayResponseType = HttpResponse<IRelative[]>;

@Injectable({ providedIn: 'root' })
export class RelativeService {
    public resourceUrl = SERVER_API_URL + 'api/relatives';

    constructor(private http: HttpClient) {}

    create(relative: IRelative): Observable<EntityResponseType> {
        return this.http.post<IRelative>(this.resourceUrl, relative, { observe: 'response' });
    }

    update(relative: IRelative): Observable<EntityResponseType> {
        return this.http.put<IRelative>(this.resourceUrl, relative, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRelative>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRelative[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
