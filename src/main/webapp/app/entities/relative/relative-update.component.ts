import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IRelative } from 'app/shared/model/relative.model';
import { RelativeService } from './relative.service';
import { IPerson } from 'app/shared/model/person.model';
import { PersonService } from 'app/entities/person';
import { IFamily } from 'app/shared/model/family.model';
import { FamilyService } from 'app/entities/family';

@Component({
    selector: 'jhi-relative-update',
    templateUrl: './relative-update.component.html'
})
export class RelativeUpdateComponent implements OnInit {
    relative: IRelative;
    isSaving: boolean;

    origins: IPerson[];

    destinations: IPerson[];

    families: IFamily[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private relativeService: RelativeService,
        private personService: PersonService,
        private familyService: FamilyService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ relative }) => {
            this.relative = relative;
        });
        this.personService.query({ filter: 'relative-is-null' }).subscribe(
            (res: HttpResponse<IPerson[]>) => {
                if (!this.relative.origin || !this.relative.origin.id) {
                    this.origins = res.body;
                } else {
                    this.personService.find(this.relative.origin.id).subscribe(
                        (subRes: HttpResponse<IPerson>) => {
                            this.origins = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.personService.query({ filter: 'relative-is-null' }).subscribe(
            (res: HttpResponse<IPerson[]>) => {
                if (!this.relative.destination || !this.relative.destination.id) {
                    this.destinations = res.body;
                } else {
                    this.personService.find(this.relative.destination.id).subscribe(
                        (subRes: HttpResponse<IPerson>) => {
                            this.destinations = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.familyService.query().subscribe(
            (res: HttpResponse<IFamily[]>) => {
                this.families = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.relative.id !== undefined) {
            this.subscribeToSaveResponse(this.relativeService.update(this.relative));
        } else {
            this.subscribeToSaveResponse(this.relativeService.create(this.relative));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRelative>>) {
        result.subscribe((res: HttpResponse<IRelative>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPersonById(index: number, item: IPerson) {
        return item.id;
    }

    trackFamilyById(index: number, item: IFamily) {
        return item.id;
    }
}
