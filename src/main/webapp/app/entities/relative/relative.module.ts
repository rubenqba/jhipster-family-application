import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JFamilyAppSharedModule } from 'app/shared';
import {
    RelativeComponent,
    RelativeDetailComponent,
    RelativeUpdateComponent,
    RelativeDeletePopupComponent,
    RelativeDeleteDialogComponent,
    relativeRoute,
    relativePopupRoute
} from './';

const ENTITY_STATES = [...relativeRoute, ...relativePopupRoute];

@NgModule({
    imports: [JFamilyAppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RelativeComponent,
        RelativeDetailComponent,
        RelativeUpdateComponent,
        RelativeDeleteDialogComponent,
        RelativeDeletePopupComponent
    ],
    entryComponents: [RelativeComponent, RelativeUpdateComponent, RelativeDeleteDialogComponent, RelativeDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JFamilyAppRelativeModule {}
