import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Relative } from 'app/shared/model/relative.model';
import { RelativeService } from './relative.service';
import { RelativeComponent } from './relative.component';
import { RelativeDetailComponent } from './relative-detail.component';
import { RelativeUpdateComponent } from './relative-update.component';
import { RelativeDeletePopupComponent } from './relative-delete-dialog.component';
import { IRelative } from 'app/shared/model/relative.model';

@Injectable({ providedIn: 'root' })
export class RelativeResolve implements Resolve<IRelative> {
    constructor(private service: RelativeService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Relative> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Relative>) => response.ok),
                map((relative: HttpResponse<Relative>) => relative.body)
            );
        }
        return of(new Relative());
    }
}

export const relativeRoute: Routes = [
    {
        path: 'relative',
        component: RelativeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jFamilyApp.relative.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'relative/:id/view',
        component: RelativeDetailComponent,
        resolve: {
            relative: RelativeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jFamilyApp.relative.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'relative/new',
        component: RelativeUpdateComponent,
        resolve: {
            relative: RelativeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jFamilyApp.relative.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'relative/:id/edit',
        component: RelativeUpdateComponent,
        resolve: {
            relative: RelativeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jFamilyApp.relative.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const relativePopupRoute: Routes = [
    {
        path: 'relative/:id/delete',
        component: RelativeDeletePopupComponent,
        resolve: {
            relative: RelativeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jFamilyApp.relative.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
