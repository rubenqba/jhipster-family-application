import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRelative } from 'app/shared/model/relative.model';
import { Principal } from 'app/core';
import { RelativeService } from './relative.service';

@Component({
    selector: 'jhi-relative',
    templateUrl: './relative.component.html'
})
export class RelativeComponent implements OnInit, OnDestroy {
    relatives: IRelative[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private relativeService: RelativeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.relativeService.query().subscribe(
            (res: HttpResponse<IRelative[]>) => {
                this.relatives = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRelatives();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRelative) {
        return item.id;
    }

    registerChangeInRelatives() {
        this.eventSubscriber = this.eventManager.subscribe('relativeListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
