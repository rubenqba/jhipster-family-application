export * from './relative.service';
export * from './relative-update.component';
export * from './relative-delete-dialog.component';
export * from './relative-detail.component';
export * from './relative.component';
export * from './relative.route';
