import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRelative } from 'app/shared/model/relative.model';

@Component({
    selector: 'jhi-relative-detail',
    templateUrl: './relative-detail.component.html'
})
export class RelativeDetailComponent implements OnInit {
    relative: IRelative;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ relative }) => {
            this.relative = relative;
        });
    }

    previousState() {
        window.history.back();
    }
}
