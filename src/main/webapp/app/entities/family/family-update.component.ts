import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IFamily } from 'app/shared/model/family.model';
import { FamilyService } from './family.service';

@Component({
    selector: 'jhi-family-update',
    templateUrl: './family-update.component.html'
})
export class FamilyUpdateComponent implements OnInit {
    family: IFamily;
    isSaving: boolean;

    constructor(private familyService: FamilyService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ family }) => {
            this.family = family;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.family.id !== undefined) {
            this.subscribeToSaveResponse(this.familyService.update(this.family));
        } else {
            this.subscribeToSaveResponse(this.familyService.create(this.family));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IFamily>>) {
        result.subscribe((res: HttpResponse<IFamily>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
