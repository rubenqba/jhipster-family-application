import { IRelative } from 'app/shared/model//relative.model';

export interface IFamily {
    id?: number;
    name?: string;
    relatives?: IRelative[];
}

export class Family implements IFamily {
    constructor(public id?: number, public name?: string, public relatives?: IRelative[]) {}
}
