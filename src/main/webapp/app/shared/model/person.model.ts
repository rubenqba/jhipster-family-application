import { Moment } from 'moment';

export const enum Sex {
    MALE = 'MALE',
    FEMALE = 'FEMALE'
}

export interface IPerson {
    id?: number;
    firstName?: string;
    lastName?: string;
    birthDay?: Moment;
    sex?: Sex;
}

export class Person implements IPerson {
    constructor(public id?: number, public firstName?: string, public lastName?: string, public birthDay?: Moment, public sex?: Sex) {}
}
