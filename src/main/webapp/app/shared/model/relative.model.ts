import { IPerson } from 'app/shared/model//person.model';
import { IFamily } from 'app/shared/model//family.model';

export const enum RelType {
    MARRIED = 'MARRIED',
    PARENT = 'PARENT',
    SIBLING = 'SIBLING'
}

export interface IRelative {
    id?: number;
    type?: RelType;
    origin?: IPerson;
    destination?: IPerson;
    family?: IFamily;
}

export class Relative implements IRelative {
    constructor(
        public id?: number,
        public type?: RelType,
        public origin?: IPerson,
        public destination?: IPerson,
        public family?: IFamily
    ) {}
}
