package com.github.rubenqba.family.service;

import com.github.rubenqba.family.domain.Relative;
import com.github.rubenqba.family.repository.RelativeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Relative.
 */
@Service
@Transactional
public class RelativeService {

    private final Logger log = LoggerFactory.getLogger(RelativeService.class);

    private final RelativeRepository relativeRepository;

    public RelativeService(RelativeRepository relativeRepository) {
        this.relativeRepository = relativeRepository;
    }

    /**
     * Save a relative.
     *
     * @param relative the entity to save
     * @return the persisted entity
     */
    public Relative save(Relative relative) {
        log.debug("Request to save Relative : {}", relative);
        return relativeRepository.save(relative);
    }

    /**
     * Get all the relatives.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Relative> findAll() {
        log.debug("Request to get all Relatives");
        return relativeRepository.findAll();
    }


    /**
     * Get one relative by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Relative> findOne(Long id) {
        log.debug("Request to get Relative : {}", id);
        return relativeRepository.findById(id);
    }

    /**
     * Delete the relative by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Relative : {}", id);
        relativeRepository.deleteById(id);
    }
}
