package com.github.rubenqba.family.domain.enumeration;

/**
 * The Sex enumeration.
 */
public enum Sex {
    MALE, FEMALE
}
