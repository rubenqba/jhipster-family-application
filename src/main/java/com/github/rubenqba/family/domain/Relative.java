package com.github.rubenqba.family.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import com.github.rubenqba.family.domain.enumeration.RelType;

/**
 * A Relative.
 */
@Entity
@Table(name = "relative")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Relative implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private RelType type;

    @OneToOne    @JoinColumn(unique = true)
    private Person origin;

    @OneToOne    @JoinColumn(unique = true)
    private Person destination;

    @ManyToOne
    @JsonIgnoreProperties("relatives")
    private Family family;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RelType getType() {
        return type;
    }

    public Relative type(RelType type) {
        this.type = type;
        return this;
    }

    public void setType(RelType type) {
        this.type = type;
    }

    public Person getOrigin() {
        return origin;
    }

    public Relative origin(Person person) {
        this.origin = person;
        return this;
    }

    public void setOrigin(Person person) {
        this.origin = person;
    }

    public Person getDestination() {
        return destination;
    }

    public Relative destination(Person person) {
        this.destination = person;
        return this;
    }

    public void setDestination(Person person) {
        this.destination = person;
    }

    public Family getFamily() {
        return family;
    }

    public Relative family(Family family) {
        this.family = family;
        return this;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Relative relative = (Relative) o;
        if (relative.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), relative.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Relative{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            "}";
    }
}
