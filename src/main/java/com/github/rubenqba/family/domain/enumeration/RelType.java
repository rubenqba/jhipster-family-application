package com.github.rubenqba.family.domain.enumeration;

/**
 * The RelType enumeration.
 */
public enum RelType {
    MARRIED, PARENT, SIBLING
}
