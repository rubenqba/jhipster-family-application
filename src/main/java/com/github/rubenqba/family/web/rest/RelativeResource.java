package com.github.rubenqba.family.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.github.rubenqba.family.domain.Relative;
import com.github.rubenqba.family.service.RelativeService;
import com.github.rubenqba.family.web.rest.errors.BadRequestAlertException;
import com.github.rubenqba.family.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Relative.
 */
@RestController
@RequestMapping("/api")
public class RelativeResource {

    private final Logger log = LoggerFactory.getLogger(RelativeResource.class);

    private static final String ENTITY_NAME = "relative";

    private final RelativeService relativeService;

    public RelativeResource(RelativeService relativeService) {
        this.relativeService = relativeService;
    }

    /**
     * POST  /relatives : Create a new relative.
     *
     * @param relative the relative to create
     * @return the ResponseEntity with status 201 (Created) and with body the new relative, or with status 400 (Bad Request) if the relative has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/relatives")
    @Timed
    public ResponseEntity<Relative> createRelative(@RequestBody Relative relative) throws URISyntaxException {
        log.debug("REST request to save Relative : {}", relative);
        if (relative.getId() != null) {
            throw new BadRequestAlertException("A new relative cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Relative result = relativeService.save(relative);
        return ResponseEntity.created(new URI("/api/relatives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /relatives : Updates an existing relative.
     *
     * @param relative the relative to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated relative,
     * or with status 400 (Bad Request) if the relative is not valid,
     * or with status 500 (Internal Server Error) if the relative couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/relatives")
    @Timed
    public ResponseEntity<Relative> updateRelative(@RequestBody Relative relative) throws URISyntaxException {
        log.debug("REST request to update Relative : {}", relative);
        if (relative.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Relative result = relativeService.save(relative);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, relative.getId().toString()))
            .body(result);
    }

    /**
     * GET  /relatives : get all the relatives.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of relatives in body
     */
    @GetMapping("/relatives")
    @Timed
    public List<Relative> getAllRelatives() {
        log.debug("REST request to get all Relatives");
        return relativeService.findAll();
    }

    /**
     * GET  /relatives/:id : get the "id" relative.
     *
     * @param id the id of the relative to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the relative, or with status 404 (Not Found)
     */
    @GetMapping("/relatives/{id}")
    @Timed
    public ResponseEntity<Relative> getRelative(@PathVariable Long id) {
        log.debug("REST request to get Relative : {}", id);
        Optional<Relative> relative = relativeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(relative);
    }

    /**
     * DELETE  /relatives/:id : delete the "id" relative.
     *
     * @param id the id of the relative to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/relatives/{id}")
    @Timed
    public ResponseEntity<Void> deleteRelative(@PathVariable Long id) {
        log.debug("REST request to delete Relative : {}", id);
        relativeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
