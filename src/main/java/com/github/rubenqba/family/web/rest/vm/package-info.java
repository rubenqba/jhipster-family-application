/**
 * View Models used by Spring MVC REST controllers.
 */
package com.github.rubenqba.family.web.rest.vm;
