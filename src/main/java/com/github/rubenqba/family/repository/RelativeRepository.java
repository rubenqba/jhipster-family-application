package com.github.rubenqba.family.repository;

import com.github.rubenqba.family.domain.Relative;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Relative entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RelativeRepository extends JpaRepository<Relative, Long> {

}
