/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JFamilyAppTestModule } from '../../../test.module';
import { RelativeComponent } from 'app/entities/relative/relative.component';
import { RelativeService } from 'app/entities/relative/relative.service';
import { Relative } from 'app/shared/model/relative.model';

describe('Component Tests', () => {
    describe('Relative Management Component', () => {
        let comp: RelativeComponent;
        let fixture: ComponentFixture<RelativeComponent>;
        let service: RelativeService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JFamilyAppTestModule],
                declarations: [RelativeComponent],
                providers: []
            })
                .overrideTemplate(RelativeComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RelativeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RelativeService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Relative(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.relatives[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
