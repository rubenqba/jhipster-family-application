/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { JFamilyAppTestModule } from '../../../test.module';
import { RelativeUpdateComponent } from 'app/entities/relative/relative-update.component';
import { RelativeService } from 'app/entities/relative/relative.service';
import { Relative } from 'app/shared/model/relative.model';

describe('Component Tests', () => {
    describe('Relative Management Update Component', () => {
        let comp: RelativeUpdateComponent;
        let fixture: ComponentFixture<RelativeUpdateComponent>;
        let service: RelativeService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JFamilyAppTestModule],
                declarations: [RelativeUpdateComponent]
            })
                .overrideTemplate(RelativeUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RelativeUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RelativeService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Relative(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.relative = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Relative();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.relative = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
