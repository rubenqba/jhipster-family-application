/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JFamilyAppTestModule } from '../../../test.module';
import { RelativeDetailComponent } from 'app/entities/relative/relative-detail.component';
import { Relative } from 'app/shared/model/relative.model';

describe('Component Tests', () => {
    describe('Relative Management Detail Component', () => {
        let comp: RelativeDetailComponent;
        let fixture: ComponentFixture<RelativeDetailComponent>;
        const route = ({ data: of({ relative: new Relative(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JFamilyAppTestModule],
                declarations: [RelativeDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RelativeDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RelativeDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.relative).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
