package com.github.rubenqba.family.web.rest;

import com.github.rubenqba.family.JFamilyApp;

import com.github.rubenqba.family.domain.Relative;
import com.github.rubenqba.family.repository.RelativeRepository;
import com.github.rubenqba.family.service.RelativeService;
import com.github.rubenqba.family.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.github.rubenqba.family.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.github.rubenqba.family.domain.enumeration.RelType;
/**
 * Test class for the RelativeResource REST controller.
 *
 * @see RelativeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JFamilyApp.class)
public class RelativeResourceIntTest {

    private static final RelType DEFAULT_TYPE = RelType.MARRIED;
    private static final RelType UPDATED_TYPE = RelType.PARENT;

    @Autowired
    private RelativeRepository relativeRepository;

    @Autowired
    private RelativeService relativeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRelativeMockMvc;

    private Relative relative;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RelativeResource relativeResource = new RelativeResource(relativeService);
        this.restRelativeMockMvc = MockMvcBuilders.standaloneSetup(relativeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Relative createEntity(EntityManager em) {
        Relative relative = new Relative()
            .type(DEFAULT_TYPE);
        return relative;
    }

    @Before
    public void initTest() {
        relative = createEntity(em);
    }

    @Test
    @Transactional
    public void createRelative() throws Exception {
        int databaseSizeBeforeCreate = relativeRepository.findAll().size();

        // Create the Relative
        restRelativeMockMvc.perform(post("/api/relatives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(relative)))
            .andExpect(status().isCreated());

        // Validate the Relative in the database
        List<Relative> relativeList = relativeRepository.findAll();
        assertThat(relativeList).hasSize(databaseSizeBeforeCreate + 1);
        Relative testRelative = relativeList.get(relativeList.size() - 1);
        assertThat(testRelative.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createRelativeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = relativeRepository.findAll().size();

        // Create the Relative with an existing ID
        relative.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRelativeMockMvc.perform(post("/api/relatives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(relative)))
            .andExpect(status().isBadRequest());

        // Validate the Relative in the database
        List<Relative> relativeList = relativeRepository.findAll();
        assertThat(relativeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRelatives() throws Exception {
        // Initialize the database
        relativeRepository.saveAndFlush(relative);

        // Get all the relativeList
        restRelativeMockMvc.perform(get("/api/relatives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(relative.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getRelative() throws Exception {
        // Initialize the database
        relativeRepository.saveAndFlush(relative);

        // Get the relative
        restRelativeMockMvc.perform(get("/api/relatives/{id}", relative.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(relative.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRelative() throws Exception {
        // Get the relative
        restRelativeMockMvc.perform(get("/api/relatives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRelative() throws Exception {
        // Initialize the database
        relativeService.save(relative);

        int databaseSizeBeforeUpdate = relativeRepository.findAll().size();

        // Update the relative
        Relative updatedRelative = relativeRepository.findById(relative.getId()).get();
        // Disconnect from session so that the updates on updatedRelative are not directly saved in db
        em.detach(updatedRelative);
        updatedRelative
            .type(UPDATED_TYPE);

        restRelativeMockMvc.perform(put("/api/relatives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRelative)))
            .andExpect(status().isOk());

        // Validate the Relative in the database
        List<Relative> relativeList = relativeRepository.findAll();
        assertThat(relativeList).hasSize(databaseSizeBeforeUpdate);
        Relative testRelative = relativeList.get(relativeList.size() - 1);
        assertThat(testRelative.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingRelative() throws Exception {
        int databaseSizeBeforeUpdate = relativeRepository.findAll().size();

        // Create the Relative

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRelativeMockMvc.perform(put("/api/relatives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(relative)))
            .andExpect(status().isBadRequest());

        // Validate the Relative in the database
        List<Relative> relativeList = relativeRepository.findAll();
        assertThat(relativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRelative() throws Exception {
        // Initialize the database
        relativeService.save(relative);

        int databaseSizeBeforeDelete = relativeRepository.findAll().size();

        // Get the relative
        restRelativeMockMvc.perform(delete("/api/relatives/{id}", relative.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Relative> relativeList = relativeRepository.findAll();
        assertThat(relativeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Relative.class);
        Relative relative1 = new Relative();
        relative1.setId(1L);
        Relative relative2 = new Relative();
        relative2.setId(relative1.getId());
        assertThat(relative1).isEqualTo(relative2);
        relative2.setId(2L);
        assertThat(relative1).isNotEqualTo(relative2);
        relative1.setId(null);
        assertThat(relative1).isNotEqualTo(relative2);
    }
}
